#!/bin/bash

# Adiciona usuario zabbix no grupo docker
sudo gpasswd -a zabbix docker

# Copia Config Docker
sudo mkdir /var/lib/zabbix/.docker
cp config.json /var/lib/zabbix/.docker

# Copia scripts
sudo mkdir /etc/zabbix/scripts
cp scripts/* /etc/zabbix/scripts/

# Copia user_parameters
mkdir /etc/zabbix/zabbix_agent.d
cp userparameter_docker.conf /etc/zabbix/zabbix_agent.d/

# Reinicia zabbix agent
sudo service zabbix-agent restart
